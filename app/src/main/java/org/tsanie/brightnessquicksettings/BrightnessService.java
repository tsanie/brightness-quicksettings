package org.tsanie.brightnessquicksettings;

import android.provider.Settings;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;
import android.util.Log;

import java.io.DataOutputStream;

/**
 * Brightness Service
 *
 * Created by Tsanie on 2017/2/22.
 */

public class BrightnessService extends TileService {

    private final String TAG = "BrightHelperService";
    private final int STATE_OFF = 0;
    private final int MINIMUM_BRIGHTNESS = 1;

    private int lastBrightness = STATE_OFF;

    @Override
    public void onTileAdded() {
        getQsTile().setState(Tile.STATE_INACTIVE);
        getQsTile().updateTile();
    }

    @Override
    public void onClick() {
        Log.d(TAG, "onClick");

//        if (!CheckPermission()) {
//            return;
//        }

        int brightness;

        if (lastBrightness == STATE_OFF) {
            // need to dark backlight
            try {
                lastBrightness = Settings.System.getInt(getContentResolver(),
                        Settings.System.SCREEN_BRIGHTNESS,
                        MINIMUM_BRIGHTNESS);
                Log.d(TAG, "brightness: " + lastBrightness);
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                return;
            }
            brightness = STATE_OFF;
            getQsTile().setState(Tile.STATE_ACTIVE);
        } else {
            brightness = lastBrightness;
            lastBrightness = STATE_OFF;
            getQsTile().setState(Tile.STATE_INACTIVE);
        }

        getQsTile().updateTile();

        Process process = null;
        DataOutputStream os = null;

        try {
            String cmd = "echo " + brightness + " > /sys/class/leds/lcd-backlight/brightness";
            process = Runtime.getRuntime().exec("su");

            os = new DataOutputStream(process.getOutputStream());
            os.writeBytes(cmd + "\n");
            os.writeBytes("exit\n");
            os.flush();
            process.waitFor();
        } catch (Exception e) {
            Log.w(TAG, "Error to su: " + e.toString());
        } finally {
            try {
                if (os != null) {
                    os.close();
                }
                process.destroy();
            } catch (Exception e) {
                Log.e(TAG, "Error to destroy process: " + e.toString());
            }
        }
    }

//    private boolean CheckPermission() {
//        Context context = getApplicationContext();
//
//        if (!Settings.System.canWrite(context)) {
//            Uri selfPackageUri = Uri.parse("package:" + context.getPackageName());
//            Intent intent = new Intent(Settings.ACTION_MANAGE_WRITE_SETTINGS, selfPackageUri);
//            startActivity(intent);
//
//            return false;
//        }
//
//        return true;
//    }
}
